package Arvore;

import javax.swing.tree.DefaultMutableTreeNode;

public class No extends DefaultMutableTreeNode{

    public No(Object userObject) {
        super(userObject);
    }
  
    public void addNo(No no) {
        this.add(no);
    }

}
