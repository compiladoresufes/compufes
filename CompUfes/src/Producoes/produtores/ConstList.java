/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle - Pronto de Compiladores 2
 */
public class ConstList extends Production {

    public ConstList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("COMMA");
    }

    @Override
    public boolean accept(String token) {
        return this.nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        if (this.nomes.get(0).equals(token)) {
            No no_1 = new No("COMMA");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<constDeclarator>");
            no.addNo(no_2);
            this.codigo = new ConstDeclarator(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                ConstList cl = new ConstList(tokens);
                if (cl.accept(token)) {
                    No no_3 = new No("<constList>");
                    no.addNo(no_3);
                    this.codigo = cl.handle(tokens, no_3);
                }
            }
        }
        return this.codigo;
    }

}
