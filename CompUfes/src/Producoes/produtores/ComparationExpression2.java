/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle - Pronto de Compiladores 2
 */
public class ComparationExpression2 extends Production {

    public ComparationExpression2(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ASSIGN");
        nomes.add("NOTEQUAL");
        nomes.add("GT");
        nomes.add("GTE");
        nomes.add("LT");
        nomes.add("LTE");
        nomes.add("NOT");
    }

    @Override
    public boolean accept(String token) {
        return true; //Pq aceita vazio
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        if (nomes.contains(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<operationalExpression>");
            no.addNo(no_2);
            this.codigo = new OperationExpression(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                No no_3 = new No("<comparationExpression2>");
                no.addNo(no_3);
                this.codigo = new ComparationExpression2(tokens).handle(tokens, no_3);
            }
        }
        return this.codigo;
    }

}
