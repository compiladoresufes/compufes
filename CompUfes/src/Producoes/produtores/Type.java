/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle comp 2
 */
public class Type extends Production {

    public Type(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("INTEGERDECLARE");
        nomes.add("REALDECLARE");
        nomes.add("BOOLEANDECLARE");
        nomes.add("CHARDECLARE");
        nomes.add("STRINGDECLARE");
        nomes.add("STRUCTDECLARE");        
    }

    @Override
    public boolean accept(String token) {
        return this.nomes.contains(token) || new Pointer(tokens).accept(token);
    }
    
    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No(token);
            no.addNo(no_1);
            tokens.removeFirst();
        } else if(new Pointer(tokens).accept(token)){
            No no_1 = new No("<pointer>");
            no.addNo(no_1);
            this.codigo = new Pointer(tokens).handle(tokens, no_1);
        }else{
            this.codigo = 13;
        }
        return this.codigo;
    }
}
