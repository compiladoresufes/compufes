/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class StructDeclarators extends Production {

    public StructDeclarators(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("STRUCTDECLARE");
        nomes.add("TWOPOINTS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        No no_1 = new No(token);
        no.addNo(no_1);
        tokens.removeFirst();
        token = tokens.getFirstToken();
        if (nomes.get(1).equals(token)) {
            No no_2 = new No("TWOPOINTS");
            no.addNo(no_2);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            StructList structList = new StructList(tokens);
            if (structList.accept(token)) {
                No no_3 = new No("<structList>");
                no.addNo(no_3);
                this.codigo = structList.handle(tokens, no_3);
            }
        }

        return this.codigo;
    }

}
