/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Gustavo
 */
public class FunctionList extends Production {

    public FunctionList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ID");
        nomes.add("LEFTKEY");
        nomes.add("LEFTTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
        nomes.add("RIGHTKEY");
        nomes.add("RETURN");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
            if (nomes.get(2).equals(token)) {
                No no_4 = new No("LEFTTPARENTESIS");
                no.addNo(no_4);
                tokens.removeFirst();
                No no_5 = new No("<variableDeclarator>");
                no.addNo(no_5);
                this.codigo = new VariableDeclarator(tokens).handle(tokens, no_5);
                if (this.codigo == 0) {
                    token = tokens.getFirstToken();
                    if (nomes.get(3).equals(token)) {
                        No no_6 = new No("RIGHTPARENTESIS");
                        no.addNo(no_6);
                        tokens.removeFirst();
                        No no_7 = new No("<returnList>");
                        no.addNo(no_7);
                        this.codigo = new ReturnList(tokens).handle(tokens, no_7);
                        if (this.codigo == 0) {
                            No no_8 = new No("LEFTKEY");
                            no.addNo(no_8);
                            tokens.removeFirst();
                            No no_9 = new No("<variableDeclarator>");
                            no.addNo(no_9);
                            this.codigo = new VariableDeclarator(tokens).handle(tokens, no_9);
                            if (this.codigo == 0) {
                                No no_12 = new No("<block>");
                                no.addNo(no_12);
                                this.codigo = new Block(tokens).handle(tokens, no_12);
                                if (this.codigo == 0) {
                                    token = tokens.getFirstToken();
                                    if (nomes.get(3).equals(token)) {
                                        No no_10 = new No("RIGHTKEY");
                                        no.addNo(no_10);
                                        tokens.removeFirst();
                                        No no_13 = new No("return");
                                        no.addNo(no_13);
                                        tokens.removeFirst();
                                        No no_14 = new No(tokens.getSecondToken());
                                        no.addNo(no_14);
                                        tokens.removeFirst();
                                        No no_11 = new No("<functionList>");
                                        no.addNo(no_11);
                                        this.codigo = new FunctionList(tokens).handle(tokens, no_11);
                                    } else {
                                        this.codigo = 12;
                                    }
                                }
                            } else {
                                this.codigo = 11;
                            }
                        } else {
                            this.codigo = 12;
                        }
                    }
                } else {
                    this.codigo = 11;
                }
            }
        }
        return this.codigo;
    }
}
