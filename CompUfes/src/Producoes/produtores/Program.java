/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Program extends Production {

    public Program(ClassWithTokens tokens) {
        super(tokens);
    }

    @Override
    public int nextProduction(No no) {
        LibraryDeclarators ld = new LibraryDeclarators(tokens);
        ConstDeclarators cd = new ConstDeclarators(tokens);
        StructDeclarators sd = new StructDeclarators(tokens);
        VariableDeclarators vd = new VariableDeclarators(tokens);
        FunctionDeclarators fd = new FunctionDeclarators(tokens);
        Main main = new Main(tokens);
        
        String token = tokens.getFirstToken();

        if (ld.accept(token) || cd.accept(token) || sd.accept(token) || vd.accept(token) || fd.accept(token) || main.accept(token)){
            if (ld.accept(token)) {
                No novoNo = new No("<libraryDeclarators>");
                no.addNo(novoNo);
                this.codigo = ld.handle(tokens,novoNo);
            }
            
            token = tokens.getFirstToken();
            if (cd.accept(token) && this.codigo == 0) {
                No novoNo = new No("<constDeclarators>");
                no.addNo(novoNo);
                this.codigo = cd.handle(tokens,novoNo);
            }
            
            token = tokens.getFirstToken();
            if (sd.accept(token) && this.codigo == 0) {
                No novoNo = new No("<structDeclarators>");
                no.addNo(novoNo);
                this.codigo = sd.handle(tokens,novoNo);
            }
            
            token = tokens.getFirstToken();
            if (vd.accept(token) && this.codigo == 0) {
                No novoNo = new No("<variableDeclarators>");
                no.addNo(novoNo);
                this.codigo = vd.handle(tokens,novoNo);
            }
            
            token = tokens.getFirstToken();
            if (main.accept(token) && this.codigo == 0) {
                No novoNo = new No("<main>");
                no.addNo(novoNo);
                this.codigo = main.handle(tokens,novoNo);
            }
        } else if(!tokens.isEmpity()) {
            this.codigo = 34;
        }
        return this.codigo;
    }

}
/*testeCommit*/