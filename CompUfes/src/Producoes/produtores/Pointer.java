/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Pointer extends Production {

    public Pointer(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("POINTERDECLARE");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("POINTER_");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            Type type = new Type(tokens);
            if (type.accept(token)) {
                No no_2 = new No("<type>");
                no.addNo(no_2);
                this.codigo = type.handle(tokens, no_2);
            }
        } else {
            this.codigo = -1;
        }
        return this.codigo;
    }

}
