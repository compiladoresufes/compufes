/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *

 * @author Egle comp 2
 */
public class SwitchList extends Production {

    public SwitchList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("CASE");
        this.nomes.add("INT");
        this.nomes.add("TWOPOINTS");
    }

    @Override
    public boolean accept(String token) {
        return true; //Pq aceita vazio

    }

    @Override
    public int nextProduction(No no) {

        String token = tokens.getFirstToken();

        if (nomes.get(0).equals(token)) {
            No no_1 = new No("CASE");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(1).equals(token)) {
                No no_2 = new No("INT");
                no.addNo(no_2);
                tokens.removeFirst();
                token = tokens.getFirstToken();
                if (nomes.get(2).equals(token)) {
                    No no_3 = new No("INT");
                    no.addNo(no_3);
                    tokens.removeFirst();
                    No no_4 = new No("<block>");
                    no.addNo(no_4);
                    this.codigo = new Block(tokens).handle(tokens, no_4);
                    if (this.codigo == 0) {
                        No no_5 = new No("<switchList2>");
                        no.addNo(no_5);
                        this.codigo = new SwitchList2(tokens).handle(tokens, no_5);
                    }else{
                        this.codigo = -1;
                    }
                } else {
                    this.codigo = -1;
                }
            } else {
                this.codigo = -1;
            }
        } else {
            this.codigo = -1;
        }
        return this.codigo;
    }


}
