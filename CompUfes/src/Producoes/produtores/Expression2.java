/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Expression2 extends Production {

    public Expression2(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("OR");
        nomes.add("AND");
    }    

    @Override
    public boolean accept(String token) {
        return true;
    }
    
    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No(token);
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<comparationExpression>");
            no.addNo(no_2);
            this.codigo = new ComparationExpression(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                No no_3 = new No("<expression2>");
                no.addNo(no_3);
                this.codigo = new Expression2(tokens).handle(tokens, no_3);
            }
        }
        return this.codigo;
    }

}
