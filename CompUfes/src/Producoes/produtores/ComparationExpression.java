/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle - Pronto de Compiladores 2
 */
public class ComparationExpression extends Production {

    public ComparationExpression(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return new OperationExpression2(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<operationalExpression>");
        no.addNo(no_1);
        this.codigo = new OperationExpression2(tokens).handle(tokens, no_1);
        if (this.codigo == 0) {
            No no_2 = new No("<comparationExpression2>");
            no.addNo(no_2);
            this.codigo = new ComparationExpression2(tokens).handle(tokens, no_2);
        }
        return this.codigo;
    }
}
