/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Gustavo
 */
public class LibraryList extends Production {

    public LibraryList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("COMMA");
        nomes.add("ID");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        No no_2 = new No("COMMA");
        no.addNo(no_2);
        tokens.removeFirst();
        token = tokens.getFirstToken();
        if (nomes.get(1).equals(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            LibraryList ll = new LibraryList(tokens);
            if (ll.accept(token)) {
                No no_3 = new No("<libraryList>");
                no.addNo(no_3);
                this.codigo = ll.handle(tokens, no_3);
            }
        } else {
            this.codigo = -1;
        }

        return this.codigo;
    }

}
