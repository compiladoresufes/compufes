/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ReturnList2 extends Production {

    public ReturnList2(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("COMMA");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("ASSIGN");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            Type type = new Type(tokens);
            if (type.accept(token)) {
                No no_2 = new No("<type>");
                no.addNo(no_2);
                this.codigo = type.handle(tokens, no_2);
                if(this.codigo == 0){
                    ReturnList2 returnList2 = new ReturnList2(tokens);
                    if (returnList2.accept(token)) {
                        No no_3 = new No("<returnList2>");
                        no.addNo(no_3);
                        this.codigo = returnList2.handle(tokens, no_2);
                    }
                } else {
                    this.codigo = -1;
                }
            }
        } 
        
        return this.codigo;
    }

}
