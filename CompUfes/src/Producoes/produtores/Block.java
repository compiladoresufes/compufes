/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle - Pronto de Compiladores 2
 */
public class Block extends Production {

    public Block(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return true; //Por que aceita vazio // Se tiver outra produção então verifica se aceita do próximo será no nextProdution
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        Instruction in = new Instruction(tokens);
        
        if (in.accept(token)) {
            No no_1 = new No("<instruction>");
            no.addNo(no_1);
            this.codigo = in.handle(tokens, no_1);
            if (this.codigo == 0) {
                No no_2 = new No("<block>");
                no.addNo(no_2);
                this.codigo = new Block(tokens).handle(tokens, no_2);
            }
        }
        return this.codigo;
    }
}
