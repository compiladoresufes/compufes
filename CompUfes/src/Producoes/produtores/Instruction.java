/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

import java.util.ArrayList;

/**
 *
 * @author Gustavo
 */
public class Instruction extends Production {

    private ArrayList<String> nomesIniciais;

    public Instruction(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ASSIGN");
        nomes.add("ID");
        nomes.add("SEMICOLON");
        nomes.add("LEFTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
        nomes.add("LEFTBRACKET");
        nomes.add("RIGHTBRACKET");
        nomes.add("LEFTKEY");
        nomes.add("RIGHTKEY");
        nomes.add("IF");
        nomes.add("ELSE");
        nomes.add("FOR");
        nomes.add("WHILE");
        nomes.add("REPEAT");
        nomes.add("SWITCH");
        nomes.add("BREAK");
        nomes.add("CONTINUE");
        nomesIniciais.add("ID");
        nomesIniciais.add("IF");
        nomesIniciais.add("FOR");
        nomesIniciais.add("WHILE");
        nomesIniciais.add("REPEAT");
        nomesIniciais.add("SWITCH");
        nomesIniciais.add("CONTINUE");
        nomesIniciais.add("LEFTKEY");

    }

    @Override
    public int nextProduction(No no) {

        String token = tokens.getFirstToken();
        if (nomesIniciais.contains(token)) {
            if (token.equals("ID")) {
                No no_1 = new No(tokens.getSecondToken());
                no.addNo(no_1);
                tokens.removeFirst();
                if (no_1.equals("ASSIGN")) {
                    No no_2 = new No(tokens.getFirstToken());
                    no.addNo(no_2);
                    tokens.removeFirst();
                    No no_3 = new No("<expression>");
                    no.addNo(no_3);
                    this.codigo = new Expression(tokens).handle(tokens, no_3);
                    if (this.codigo == 0) {
                        No no_4 = new No("SEMICOLON");
                        no.addNo(no_4);
                        tokens.removeFirst();
                    }
                } else if (no_1.equals("LEFTBRACKET")) {
                    No no_5 = new No(tokens.getFirstToken());
                    no.addNo(no_5);
                    tokens.removeFirst();
                    No no_6 = new No("<operationExpression>");
                    no.addNo(no_6);
                    this.codigo = new OperationExpression(tokens).handle(tokens, no_6);
                    if (this.codigo == 0) {

                        No no_7 = new No("RIGHTBRACKET");
                        no.addNo(no_7);
                        tokens.removeFirst();
                        No no_8 = new No(tokens.getFirstToken());
                        no.addNo(no_8);
                        tokens.removeFirst();
                        if (no_8.equals("ASSIGN")) {
                            No no_9 = new No(tokens.getFirstToken());
                            no.addNo(no_9);
                            tokens.removeFirst();
                            No no_10 = new No("<expression>");
                            no.addNo(no_10);
                            this.codigo = new Expression(tokens).handle(tokens, no_10);
                            if (this.codigo == 0) {
                                No no_11 = new No("SEMICOLON");
                                no.addNo(no_11);
                                tokens.removeFirst();
                            }
                        }
                    } else {
                        this.codigo = 35;
                    }

                }
                this.codigo = 36;
            } else if (token.equals("IF")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_15 = new No("LEFTPARENTESIS");
                no.add(no_15);
                tokens.removeFirst();
                No no_16 = new No("<expression>");
                no.addNo(no_16);
                this.codigo = new Expression(tokens).handle(tokens, no_16);
                if (this.codigo == 0) {
                    No no_17 = new No("RIGHTPARENTESIS");
                    no.addNo(no_17);
                    tokens.removeFirst();
                    No no_18 = new No("LEFTKEY");
                    no.addNo(no_18);
                    tokens.removeFirst();
                    No no_19 = new No("<instruction>");
                    no.addNo(no_19);
                    this.codigo = new Instruction(tokens).handle(tokens, no_19);
                    if (this.codigo == 0) {
                        No no_20 = new No("RIGHTKEY");
                        no.addNo(no_20);
                        tokens.removeFirst();
                        No no_21 = new No(tokens.getFirstToken());
                        if (no_21.equals("ELSE")) {
                            tokens.removeFirst();
                            No no_22 = new No("LEFTKEY");
                            no.addNo(no_22);
                            tokens.removeFirst();
                            No no_23 = new No("<instruction>");
                            no.addNo(no_23);
                            this.codigo = new Instruction(tokens).handle(tokens, no_23);
                            if (this.codigo == 0) {
                                No no_24 = new No("RIGHTKEY");
                                no.addNo(no_24);
                                tokens.removeFirst();
                            }
                        }
                    }
                }
            } else if (token.equals("SWITCH")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_25 = new No("LEFTPARENTESIS");
                no.add(no_25);
                tokens.removeFirst();
                No no_26 = new No(tokens.getSecondToken());
                no.add(no_26);
                tokens.removeFirst();
                No no_27 = new No("RIGHTPARENTESIS");
                no.add(no_27);
                tokens.removeFirst();
                No no_28 = new No("LEFTKEY");
                no.add(no_28);
                tokens.removeFirst();
                No no_29 = new No("<switchList>");
                no.add(no_29);
                this.codigo = new SwitchList(tokens).handle(tokens, no_29);
                if (this.codigo == 0) {
                    No no_30 = new No("<defaultSwitch>");
                    no.add(no_30);
                    this.codigo = new DefaultSwitch(tokens).handle(tokens, no_30);
                    No no_31 = new No("RIGHTKEY");
                    no.addNo(no_31);
                    tokens.removeFirst();
                }
            } else if (token.equals("WHILE")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_32 = new No("LEFTPARENTESIS");
                no.add(no_32);
                tokens.removeFirst();
                No no_33 = new No("<expression>");
                no.add(no_33);
                this.codigo = new Expression(tokens).handle(tokens, no_33);
                if (this.codigo == 0) {
                    No no_34 = new No("RIGHTPARENTESIS");
                    no.add(no_34);
                    tokens.removeFirst();
                    No no_35 = new No("LEFTKEY");
                    no.add(no_35);
                    tokens.removeFirst();
                    No no_36 = new No("<instruction>");
                    no.add(no_36);
                    this.codigo = new Instruction(tokens).handle(tokens, no_36);
                    if (this.codigo == 0) {
                        No no_37 = new No("RIGHTKEY");
                        no.add(no_37);
                        tokens.removeFirst();
                    }
                }
            } else if (token.equals("REPEAT")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_38 = new No("LEFTKEY");
                no.add(no_38);
                tokens.removeFirst();
                No no_39 = new No("<instruction>");
                no.add(no_39);
                this.codigo = new Instruction(tokens).handle(tokens, no_39);
                if (this.codigo == 0) {
                    No no_40 = new No("RIGHTKEY");
                    no.add(no_40);
                    tokens.removeFirst();
                    No no_41 = new No("WHILE");
                    no.add(no_41);
                    tokens.removeFirst();
                    No no_42 = new No("LEFTPARENTESIS");
                    no.add(no_42);
                    tokens.removeFirst();
                    No no_43 = new No("<expression>");
                    no.add(no_43);
                    this.codigo = new Expression(tokens).handle(tokens, no_43);
                    if (this.codigo == 0) {
                        No no_44 = new No("RIGHTPARENTESIS");
                        no.add(no_44);
                        tokens.removeFirst();
                        No no_45 = new No("SEMICOLON");
                        no.add(no_45);
                        tokens.removeFirst();
                    }
                }
            } else if (token.equals("FOR")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_2 = new No("LEFTPARENTESIS");
                no.addNo(no_2);
                tokens.removeFirst();
                No no_3 = new No(tokens.getSecondToken());
                no.addNo(no_3);
                tokens.removeFirst();
                No no_4 = new No("ASSIGN");
                no.addNo(no_4);
                tokens.removeFirst();
                No no_5 = new No("INT");
                no.addNo(no_5);
                tokens.removeFirst();
                No no_6 = new No("DOTS");
                no.addNo(no_6);
                tokens.removeFirst();
                No no_7 = new No("INT");
                no.addNo(no_7);
                tokens.removeFirst();
                No no_8 = new No("RIGHTPARENTESIS");
                no.addNo(no_8);
                tokens.removeFirst();
                No no_9 = new No("LEFTKEY");
                no.addNo(no_9);
                tokens.removeFirst();
                No no_10 = new No("<block>");
                no.addNo(no_10);
                this.codigo = new Block(tokens).handle(tokens, no_10);
                if (this.codigo == 0) {
                    No no_11 = new No("RIGHTKEY");
                    no.addNo(no_11);
                    tokens.removeFirst();
                }

            } else if (token.equals("CONTINUE")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_2 = new No("SEMICOLON");
                no.addNo(no_2);
                tokens.removeFirst();
            } else if (token.equals("BREAK")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_2 = new No("SEMICOLON");
                no.addNo(no_2);
                tokens.removeFirst();
            } else if (token.equals("LEFTKEY")) {
                No no_1 = new No(tokens.getFirstToken());
                no.addNo(no_1);
                tokens.removeFirst();
                No no_2 = new No("<block>");
                no.addNo(no_2);
                this.codigo = new Block(tokens).handle(tokens, no_2);
                if (this.codigo == 0) {
                    No no_3 = new No("RIGHTKEY");
                    no.addNo(no_3);
                    tokens.removeFirst();
                }
            }
        } else {
            this.codigo = 100;
        }
        return this.codigo;

    }

}
