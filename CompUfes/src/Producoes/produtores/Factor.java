/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Gustavo
 */
public class Factor extends Production {

    public Factor(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("LEFTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
        nomes.add("REAL");
        nomes.add("INT");
        nomes.add("CHAR");
        nomes.add("STRING");
    }

    @Override
    public boolean accept(String token) {
        return new Variable(tokens).accept(token) || (nomes.contains(token) && !nomes.get(1).equals(token));
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        Expression ex = new Expression(tokens);
        Variable vb = new Variable(tokens);

        if (token.equals(nomes.get(0))) {
            No no_1 = new No("LEFTPARENTESIS");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<expression>");
            no.addNo(no_2);
            this.codigo = ex.handle(tokens, no_2);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (token.equals(nomes.get(1))) {
                    token = tokens.getFirstToken();
                    No no_3 = new No("RIGHTPARENTESIS");
                    no.addNo(no_3);
                    tokens.removeFirst();
                } else {
                    this.codigo = -1;
                }
            }
        } else if (vb.accept(token)) {
            No no_2 = new No("<variable>");
            no.addNo(no_2);
            this.codigo = ex.handle(tokens, no_2);
        } else if (nomes.contains(token) && !nomes.get(0).equals(token) && !nomes.get(1).equals(token)) {
            No no_3 = new No(token);
            no.addNo(no_3);
            tokens.removeFirst();
        }else{
            this.codigo = -1;
        }

        return this.codigo;
    }
}
