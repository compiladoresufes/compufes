/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Avell B154 PLUS
 */
public class Term2 extends Production {

    public Term2(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("MULT");
        this.nomes.add("DIV");
        this.nomes.add("MOD");
    }

    @Override
    public boolean accept(String token) {
        return true;
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no1 = new No(token);
            no.addNo(no1);
            tokens.removeFirst();
            No no2 = new No("<unary>");
            no.addNo(no2);
            this.codigo = new Unary(tokens).handle(tokens, no2);
            if (this.codigo == 0) {
                No no3 = new No("<term2>");
                no.addNo(no3);
                this.codigo = new Term2(tokens).handle(tokens, no3);
            }
        }
        return this.codigo;
    }

}
