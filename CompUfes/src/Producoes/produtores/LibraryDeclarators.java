/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Gustavo
 */
public class LibraryDeclarators extends Production {

    public LibraryDeclarators(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("LIBRARYDECLARE");
        nomes.add("TWOPOINTS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("LIBRARYDECLARE");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(1).equals(token)) {
                No no_2 = new No("TWOPOINTS");
                no.addNo(no_2);
                tokens.removeFirst();
                No no_3 = new No("<libraryDeclaratorsList>");
                no.addNo(no_3);
                this.codigo = new LibraryDeclaratorsList(tokens).handle(tokens, no_3);
            }
        }
        return this.codigo;
    }
}
