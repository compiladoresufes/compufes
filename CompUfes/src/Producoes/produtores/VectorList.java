/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Avell B154 PLUS
 */
public class VectorList extends Production{

    public VectorList(ClassWithTokens tokens) {
        super(tokens);  
        this.codigo = 0;
        this.nomes.add("COMMA");
    }
    @Override
    public boolean accept(String token){
        return true;
    }
    
    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if(this.nomes.get(0).equals(token)){
            No no1 = new No(this.nomes.get(0));
            no.addNo(no1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            Term t = new Term(tokens);
            if(t.accept(token)){
                No no2 = new No("<term>");
                no.addNo(no2);
                this.codigo = t.handle(tokens, no2);
                token = tokens.getFirstToken();
                VectorList vl = new VectorList(tokens);
                if(vl.accept(token)){
                    No no3 = new No("<vectorList>");
                    no.addNo(no3);
                    this.codigo = vl.handle(tokens, no3);
                }
            }
        }
        return this.codigo;
    }
    
}
