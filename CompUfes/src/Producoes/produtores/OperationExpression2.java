/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class OperationExpression2 extends Production {

    public OperationExpression2(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return true;
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<term>");
        no.addNo(no_1);
        Term t = new Term(tokens);
        this.codigo = t.handle(tokens, no_1);
        if(this.codigo == 0){
            OperationExpression2 oe2 = new OperationExpression2(tokens);
            String token = tokens.getFirstToken();
            if(oe2.accept(token)){
                No no_2 = new No("<operationExpression2>");
                no.addNo(no_2);
                this.codigo = oe2.handle(tokens, no_2);
            }
        }
        
        return this.codigo;
    }

}
