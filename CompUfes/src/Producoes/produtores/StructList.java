/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class StructList extends Production {

    public StructList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ID");
        nomes.add("LEFTKEY");
        nomes.add("RIGHTKEY");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if(nomes.get(1).equals(token)){
                No no_2 = new No("LEFTKEY");
                no.addNo(no_2);
                tokens.removeFirst();
                VariableDeclarators vd = new VariableDeclarators(tokens);
                token = tokens.getFirstToken();
                if (vd.accept(token)) {
                    No no_3 = new No("<variableDeclarators>");
                    no.addNo(no_3);
                    this.codigo = vd.handle(tokens, no_3);
                    if(this.codigo == 0){
                        token = tokens.getFirstToken();
                        if(nomes.get(2).equals(token)){
                            No no_4 = new No("RIGHTKEY");
                            no.addNo(no_4);
                            tokens.removeFirst();
                            token = tokens.getFirstToken();
                            StructList structList = new StructList(tokens);
                            if (structList.accept(token)) {
                                No no_5 = new No("<structList>");
                                no.addNo(no_5);
                                this.codigo = structList.handle(tokens, no_5);
                            }
                        } else {
                            this.codigo = -1;
                        }
                    }
                }
            } else {
                this.codigo = -1;
            }
        } 
        
        return this.codigo;
    }

}
