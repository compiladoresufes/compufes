/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Avell B154 PLUS
 */
public class VariableList extends Production {

    public VariableList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("COMMA");
        this.nomes.add("ID");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        No no1 = new No(this.nomes.get(0));
        no.addNo(no1);
        tokens.removeFirst();
        token = tokens.getFirstToken();
        if (this.nomes.get(1).equals(token)) {
            No no2 = new No(tokens.getSecondToken());
            no.addNo(no2);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            VectorDeclaration vd = new VectorDeclaration(tokens);
            if (vd.accept(token)) {
                No no3 = new No("<vectorDeclaration");
                no.addNo(no3);
                this.codigo = vd.handle(tokens, no3);
            }
            if(this.codigo == 0){
                token = tokens.getFirstToken();
                VariableList vl = new VariableList(tokens);
                if(vl.accept(token)){
                    No no4 = new No("<variableList>");
                    no.addNo(no4);
                    this.codigo = vl.handle(tokens, no4);
                }
            }
        }
        
        return this.codigo;
    }
}
