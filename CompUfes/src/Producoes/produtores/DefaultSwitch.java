/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle
 */
public class DefaultSwitch extends Production{

    public DefaultSwitch(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("DEFAULT");
        this.nomes.add("TWOPOINTS");
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        if (nomes.get(0).equals(token)) {
            No no_1 = new No("DEFAULT");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(1).equals(token)) {
                No no_2 = new No(":");
                no.addNo(no_2);
                tokens.removeFirst();
                No no_3 = new No("<block>");
                no.addNo(no_3);
                this.codigo = new Block(tokens).handle(tokens, no_3);
            } else {
                this.codigo = -1;
            }
        } else {
            this.codigo = -1;
        }
        return this.codigo;
    }
}
