/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Gustavo
 */
public class Main extends Production{

    public Main(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("MAIN");
        nomes.add("TWOPOINTS");
    }
    
    @Override
    public boolean accept(String token) {
        return new Block(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        No no_1 = new No(token);
        no.addNo(no_1);
        tokens.removeFirst();
        token = tokens.getFirstToken();
        if(nomes.get(1).equals(tokens)){
            No no_2 = new No(token);
            no.addNo(no_2);
            tokens.removeFirst();
            No no_3 = new No("<block>");
            no.addNo(no_3);
            Block vd = new Block(tokens);
            this.codigo = vd.handle(tokens, no_3);
        } else {
            this.codigo = -1;
        }
        
        return this.codigo;
    }
}
