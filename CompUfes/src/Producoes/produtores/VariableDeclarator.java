/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class VariableDeclarator extends Production {

    public VariableDeclarator(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("TWOPOINTS");
        this.nomes.add("ID");
        this.nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return new Type(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        Type t = new Type(tokens);
        No no1 = new No("<type>");
        no.addNo(no1);
        this.codigo = t.handle(tokens, no1);
        if (this.codigo == 0) {
            token = tokens.getFirstToken();
            if (this.nomes.get(0).equals(token)) {
                No no2 = new No(nomes.get(0));
                no.addNo(no2);
                tokens.removeFirst();
                token = tokens.getFirstToken();
                if (this.nomes.get(1).equals(token)) {
                    No no3 = new No(tokens.getSecondToken());
                    no.addNo(no3);
                    tokens.removeFirst();
                    token = tokens.getFirstToken();
                    VectorDeclaration vd = new VectorDeclaration(tokens);
                    if (vd.accept(token)) {
                        No no4 = new No("<vectorDeclaration>");
                        no.addNo(no4);
                        this.codigo = vd.handle(tokens, no4);
                    }
                    VariableList vl = new VariableList(tokens);
                    token = tokens.getFirstToken();
                    if (vl.accept(token) && this.codigo == 0) {
                        No no5 = new No("<variableList>");
                        no.addNo(no5);
                        this.codigo = vl.handle(tokens, no5);
                    }
                    token = tokens.getFirstToken();
                    if (this.nomes.get(2).equals(token) && this.codigo == 0) {
                        No no6 = new No(nomes.get(2));
                        no.add(no6);
                        tokens.removeFirst();
                        token = tokens.getFirstToken();
                        VariableDeclarator vad = new VariableDeclarator(tokens);
                        if (vad.accept(token)) {
                            No no7 = new No("<variableDeclarator>");
                            no.add(no7);
                            this.codigo = vad.handle(tokens, no7);
                        }

                    } else {
                        this.codigo = -1; // Faltando ;
                    }

                } else {
                    this.codigo = -1; // Faltando ID
                }
            } else {
                this.codigo = -1; //Faltando :
            }
        }

        return this.codigo;
    }
}
