/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Gustavo
 */
public class FunctionDeclarators extends Production{

    public FunctionDeclarators(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("FUNCTION");
        nomes.add("TWOPOINTS");
    }
    @Override
    public boolean accept(String token) {
        return new FunctionList(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No("FUNCTION");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("TWOPOINTS");
            no.addNo(no_2);
            tokens.removeFirst();
            No no_3 = new No("<functionList>");
            no.addNo(no_3);
            this.codigo = new FunctionList(tokens).handle(tokens, no_3);
        }
        return this.codigo;
    }
    
}
