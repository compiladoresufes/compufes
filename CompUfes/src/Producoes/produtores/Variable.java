/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Variable extends Production {

    public Variable(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ID");
        nomes.add("LEFTBRACKET");
        nomes.add("RIGHTBRACKET");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {

        No no_1 = new No(tokens.getSecondToken());
        no.addNo(no_1);
        tokens.removeFirst();
        String token = tokens.getFirstToken();
        if(nomes.get(1).equals(token)){
            No no_2 = new No(nomes.get(1));
            no.addNo(no_2);
            tokens.removeFirst();
            OperationExpression oe = new OperationExpression(tokens);
            token = tokens.getFirstToken();
            if(oe.accept(token)){
                No no_3 = new No("<operationExpression>");
                no.addNo(no_3);
                this.codigo = oe.handle(tokens, no_3);
                if(this.codigo == 0){
                    token = tokens.getFirstToken();
                    if(nomes.get(2).equals(token)){
                        tokens.removeFirst();
                        No no_4 = new No(nomes.get(2));
                        no.addNo(no_4);
                    } else {
                        this.codigo = -1;
                    }
                }
            }
        } else {
            this.codigo = -1;
        }
        
        return this.codigo;
    }

}
