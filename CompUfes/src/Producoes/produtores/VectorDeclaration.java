/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle
 */
public class VectorDeclaration extends Production {

    public VectorDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.nomes.add("LEFTBRACKET");
        this.nomes.add("RIGHTBRACKET");
    }

    @Override
    public boolean accept(String token) {
        return this.nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        if (nomes.get(0).equals(token)) {
            No no_1 = new No("LEFTBRACKET");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<term>");
            no.addNo(no_2);
            this.codigo = new Term(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                No no_3 = new No("<vectorList>");
                no.addNo(no_3);
                this.codigo = new Term(tokens).handle(tokens, no_3);
                if (this.codigo == 0) {
                    token = tokens.getFirstToken();
                    if (nomes.get(1).equals(token)) {
                        No no_4 = new No("RIGHTBRACKET");
                        no.addNo(no_4);
                        tokens.removeFirst();
                    } else {
                        this.codigo = -1;
                    }
                }
            }
        } else {
            this.codigo = -1;
        }
        return this.codigo;
    }

}
