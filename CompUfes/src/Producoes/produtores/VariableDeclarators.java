/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class VariableDeclarators extends Production {

    public VariableDeclarators(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("VARIABLEDECLARE");
        nomes.add("TWOPOINTS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        No no_1 = new No(token);
        no.addNo(no_1);
        tokens.removeFirst();
        token = tokens.getFirstToken();
        if(nomes.get(1).equals(token)){
            No no_2 = new No(token);
            no.addNo(no_2);
            tokens.removeFirst();
            No no_3 = new No("<variableDeclarator>");
            no.addNo(no_3);
            this.codigo = new VariableDeclarator(tokens).handle(tokens, no_3);
        } else {
            this.codigo = -1;
        }
        
        return this.codigo;
    }

}
