/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 * @author Egle - Pronto de Compiladores 2
 */
class ConstDeclaratorsList extends Production {

    public ConstDeclaratorsList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return true; //Por que aceita vazio
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        ConstDeclarator cd = new ConstDeclarator(tokens);
        ConstList cl = new ConstList(tokens);

        if (cd.accept(token)) {
            No no_1 = new No("<constDeclarator>");
            no.addNo(no_1);
            this.codigo = cd.handle(tokens, no_1);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (cl.accept(token)) {
                    No no_2 = new No("<constList>");
                    no.addNo(no_2);
                    this.codigo = cl.handle(tokens, no_2);
                    if (this.codigo == 0) {
                        token = tokens.getFirstToken();
                        if (nomes.get(0).equals(token)) {
                            No no_3 = new No("SEMICOLON");
                            no.addNo(no_3);
                            tokens.removeFirst();
                        } else {
                            this.codigo = -1;
                        }
                    }
                }
            }
        }
        return this.codigo;
    }
}
