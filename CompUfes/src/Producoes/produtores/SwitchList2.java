/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle comp 2
 */
class SwitchList2 extends Production {

    public SwitchList2(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        this.nomes.add("BREAK");
        this.nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return (new SwitchList(tokens).accept(token)) || (nomes.get(0).equals(token));
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        if (nomes.get(0).equals(token)) {
            No no_1 = new No("BREAK");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(1).equals(token)) {
                No no_2 = new No("SEMICOLON");
                no.addNo(no_2);
                tokens.removeFirst();
                No no_3 = new No("<switchList>");
                no.addNo(no_3);
                this.codigo = new SwitchList(tokens).handle(tokens, no_3);
            } else {
                this.codigo = -1;
            }
        } else if (new SwitchList(tokens).accept(token)) {
            No no_3 = new No("<switchList>");
            no.addNo(no_3);
            this.codigo = new SwitchList(tokens).handle(tokens, no_3);
        }else{
            this.codigo = -1;
        }
        return this.codigo;
    }

}
