/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;
import Producoes.produtores.*;
import compiler.Arquivo;
import View.ViewTela;
import com.sun.java.swing.plaf.windows.WindowsTreeUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultTreeCellRenderer;
import jsyntaxpane.syntaxkits.JavaSyntaxKit;

/**
 *
 * @author Avell B154 PLUS
 */
public class PresenterTela {

    private ViewTela view;
    private DefaultTableModel tmTokens;
    private DefaultTableModel tmErros;
    private Arquivo arquivo;
    private ClassWithTokens collectionTokens = ClassWithTokens.getClassWithTokens();
    private ArrayList<Production> producoes = new ArrayList<>();
    private ArrayList<String> texto;
    private No noRaiz = null;
    private JTree arvore = null;

    public PresenterTela() {
        iniciarTela();
    }

    public void iniciarTela() {
        view = new ViewTela();
        view.setTitle("Compilador - MiniJava");
        configurarTabelaDeTokens();
        configurarTabelaDeErros();
        view.getContainerDeGuias().setSelectedIndex(0);
        view.getjTreeDerivacao().setVisible(false);

        view.getBtnAnaliseSintatica().setEnabled(false);

        //Adiciona um listener ao botão
        view.getBtnAnaliseLexica().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                analiseLexica();
                view.getContainerDeGuias().setSelectedIndex(0);
                if (!containsErroDeToken(texto)) {
                    view.getBtnAnaliseSintatica().setEnabled(true);
                } else {
                    view.getBtnAnaliseSintatica().setEnabled(false);
                }
            }
        });

        view.getBtnAnaliseSintatica().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                analiseSintatica();
                view.getContainerDeGuias().setSelectedIndex(1);
                view.getBtnAnaliseSintatica().setEnabled(false);

                exibirArvoreDeDerivacao();

            }
        });

        view.getBtnExpandirArvore().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                expandirNos();

                if (arvore != null) {
                    if (arvore.getRowCount() > 0) {
                        if (arvore.isExpanded(0)) {
                            view.getBtnExpandirArvore().setText("Recolher árvore");
                        } else {
                            view.getBtnExpandirArvore().setText("Expandir árvore");
                        }
                    }
                }
            }
        });

        //Configura o textAreaAlgoritmo
        jsyntaxpane.DefaultSyntaxKit.initKit();
        JavaSyntaxKit jsk = new JavaSyntaxKit();
        view.getTxtAreaAlgoritmo().setEditorKit(jsk);

        view.setVisible(true);
        view.setLocationRelativeTo(null);
    }

    public void configurarTabelaDeTokens() {
        Object colunas[] = {"Token", "Lexema"};
        tmTokens = new DefaultTableModel(colunas, 0);
        view.getjTableTokens().setModel(tmTokens);
    }

    public void configurarTabelaDeErros() {
        Object colunas[] = {"-"};
        tmErros = new DefaultTableModel(colunas, 0);
        view.getjTableErros().setModel(tmErros);
    }

    private void exibirNaTabelaDeTokens(Collection<String> c) {
        tmTokens.setNumRows(0);
        Iterator<String> it = c.iterator();

        while (it.hasNext()) {
            String p = it.next();
            String[] linha = p.split("#");

            if (!linha[0].equals("ERROR") && !linha[0].equals("LINE")) {
                tmTokens.addRow(new Object[]{linha[0], linha[1]});
            }
        }
    }

    private void exibirErrosLexicosNaTabela(Collection<String> c) {
        tmErros.setNumRows(0);
        Iterator<String> it = c.iterator();

        while (it.hasNext()) {
            String p = it.next();
            String[] linha = p.split("#");

            if (linha[0].equals("ERROR")) {
                tmErros.addRow(new Object[]{linha[1]});
            }
        }
    }

    private void exibirErrosSintaticosNaTabela(int codigo) {
        tmErros.setNumRows(0);
        String expected = exibirErrosSemanticos(codigo);
        String found = "";
        if (codigo != 0) {
            found = ", but was found a '" + this.collectionTokens.getFirstToken() + "' on line " + this.collectionTokens.getUltimaLinha();
        }

        String error = expected + found;
        tmErros.addRow(new Object[]{error});
    }

    private void salvarArquivo() throws Exception {
        arquivo = new Arquivo("src/compiler/arq1.txt");
        arquivo.gravarArquivo(view.getTxtAreaAlgoritmo().getText());
    }

    public ClassWithTokens getTokenList() {
        return this.collectionTokens;
    }

    public void analiseLexica() {
        try {
            //Grava no arquivo o texto digitado no JTxt
            salvarArquivo();
            //Pega uma collection com os tokens e lexemas
            this.texto = arquivo.getTokenAndLexemaFromFile();
            //Preenche a collection de tokens (ClassWithTokens)
            setClassWithTokens(texto);

            exibirNaTabelaDeTokens(texto);
            exibirErrosLexicosNaTabela(texto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setClassWithTokens(Collection<String> c) {
        collectionTokens.resetTokens();
        Iterator<String> it = c.iterator();

        while (it.hasNext()) {
            String p = it.next();
            String[] linha = p.split("#");

            if (!linha[0].equals("ERROR") && !linha[0].equals("LINE")) {
                collectionTokens.addToken(linha[0]);
                collectionTokens.addToken(linha[1]);
                collectionTokens.addToken(linha[2]);
            }
        }
    }

    public boolean containsErroDeToken(Collection<String> c) {
        for (String linha : c) {
            if (linha.contains("ERRO")) {
                return true;
            }
        }
        return false;
    }

    public void exibirArvoreDeDerivacao() {
        if (this.noRaiz != null) {
            //Configura exibição da árvore na tela
            UIManager.put("Tree.expandedIcon", new WindowsTreeUI.ExpandedIcon());
            UIManager.put("Tree.collapsedIcon", new WindowsTreeUI.CollapsedIcon());
            arvore = new JTree(this.noRaiz);
            DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) arvore.getCellRenderer();
            renderer.setLeafIcon(null);
            renderer.setClosedIcon(null);
            renderer.setOpenIcon(null);
            arvore.setRootVisible(true);
            //Coloca árvore na tela
            view.getjScrollPaneArvore().remove(view.getjTreeDerivacao());
            view.getjScrollPaneArvore().setViewportView(arvore);
            expandirNos();
        }
    }

    public void expandirNos() {
        if (this.arvore != null) {
            if (this.arvore.getRowCount() > 0) {
                if (this.arvore.isExpanded(0)) {
                    for (int i = 0; i < this.arvore.getRowCount(); i++) {
                        //fecha todos os nós
                        this.arvore.collapseRow(i);
                    }
                } else {
                    for (int i = 0; i < this.arvore.getRowCount(); i++) {
                        //Expande todos os nós
                        this.arvore.expandRow(i);
                    }
                }
            }
        }
    }

    public void analiseSintatica() {
        try {
            this.noRaiz = new No("<program>");
            Program programProduction = new Program(collectionTokens);
            int cod = programProduction.handle(collectionTokens, this.noRaiz);
            exibirErrosSintaticosNaTabela(cod);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public String exibirErrosSemanticos(int codigo) {
        String error = "Cacildis, it was expected ";
        switch (codigo) {
            case 0:
                error = "\"Nossinhora\", you're the best coder that I have seen in my entire life!";
                break;
            case 1:
                error += "' ; '";
                break;
            case 2:
                error += "' package '";
                break;
            case 3:
                error += "' import '";
                break;
            case 4:
                error += "' class '";
                break;
            case 5:
                error += "class modifier";
                break;
            case 6:
                error += "' { '";
                break;
            case 7:
                error += "' } '";
                break;
            case 8:
                error += " modifier ";
                break;
            case 9:
                error += "' , '";
                break;
            case 10:
                error += "' = '";
                break;
            case 11:
                error += "' ( '";
                break;
            case 12:
                error += "' ) '";
                break;
            case 13:
                error += " type ";
                break;
            case 14:
                error += "' : '";
                break;
            case 15:
                error += "' if '";
                break;
            case 16:
                error += "' else '";
                break;
            case 17:
                error += "' while '";
                break;
            case 18:
                error += "' do '";
                break;
            case 19:
                error += "' break '";
                break;
            case 20:
                error += "' continue '";
                break;
            case 21:
                error += "' return '";
                break;
            case 22:
                error += "' try '";
                break;
            case 23:
                error += "' catch '";
                break;
            case 24:
                error += "'finally'";
                break;
            case 25:
                error = "Missing assignment operator";
                break;
            case 26:
                error += "' || '";
                break;
            case 27:
                error += "' && '";
                break;
            case 28:
                error = "Missing comparation operator";
                break;
            case 29:
                error = "Missing math operator";
                break;
            case 30:
                error += "' super '";
                break;
            case 31:
                error += "' . '";
                break;
            case 32:
                error += "' new '";
                break;
            case 33:
                error += "' for '";
                break;
            case 34:
                error += "' class '";
                break;
            case 1001:
                error = "Missing Identifier";
                break;
            case 1002:
                error = "Statement error, there is no production to go ahead";
                break;
            case 1003:
                error = "StatementExpression, there is no production to go ahead";
                break;
            case 1004:
                error = "Missing class body";
                break;
            default:
                error = "Error on Compiler";
                break;
        }

        return error;
    }

}
