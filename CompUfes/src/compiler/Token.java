/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

/**
 *
 * @author Avell B154 PLUS
 */
public enum Token {
    ID, INT, BOOLEAN, TRUE, FALSE, CHAR, STRING, REAL, POINTER_,
    CHARDECLARE, BOOLEANDECLARE, INTEGERDECLARE,
    REALDECLARE, STRINGDECLARE, POINTERDECLARE, STRUCTDECLARE, VARIABLEDECLARE,
    FUNCTIONDECLARE, LIBRARYDECLARE, MAIN,
    ERROR, ASSIGN, SUM, MULT, MINUS, MOD_, DIV, POW, EQUAL, OR_, AND_, LT,
    LTE, GTE, GT, NOT, NOTEQUAL, NULL, BREAK, CASE, CONSTANT, CONTINUE, DEFAULT,
    REPEAT, ELSE, FOR, IF, WHILE,
    RETURN, SWITCH, SEMICOLON, LEFTPARENTESIS, RIGHTPARENTESIS, LEFTBRACKET,
    RIGHTBRACKET, SINGLEQUOTE, DOUBLEQUOTE, TWOPOINTS, LEFTKEY, DOTS,
    RIGHTKEY, COMMA, LINE
    
}
