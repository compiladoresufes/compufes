package compiler;
import static compiler.Token.*;
%%
%class Lexer
%type Token
W = [a-zA-Z_]
N = [0-9]
FLit1    = {N}+ \. {N}*
FLit2    = \. {N}+
FLit3    = {N}+
TRUE = true
FALSE = false
BLANK=[ \t\r]
TraditionalComment = "/*" [^*] ~"*/" | "/*" "*"+ "/"
LineComment = "//".*
%{
public String lexema;
%}
%%
{BLANK}|{TraditionalComment}|{LineComment} {/*Ignore*/}

(\".*\") {lexema=yytext(); return STRING;}
("^(-"{N}+")")|{N}+ {lexema=yytext(); return INT;}
({FLit1}|{FLit2}|{FLit3}) {lexema=yytext(); return REAL;}
{TRUE}|{FALSE} {lexema=yytext(); return BOOLEAN;}
'.' {lexema=yytext(); return CHAR;}

"<-" {return ASSIGN;}
"pointer->" {return POINTERDECLARE;}
"+" {return SUM;}
"-" {return MINUS;}
"*" {return MULT;}
"^" {return POW;}
"/" {return DIV;}
"=" {return EQUAL;}
"or" {return OR_;}
"and" {return AND_;}
"~" {return NOT;}
"!=" {return NOTEQUAL;}
"<" {return LT;}
"<=" {return LTE;}
">" {return GT;}
">=" {return GTE;}
"mod" {return MOD_;}
"null" {return NULL;}
"break" {return BREAK;}
"case" {return CASE;}
"char" {return CHARDECLARE;}
"boolean" {return BOOLEANDECLARE;}
"integer" {return INTEGERDECLARE;}
"real" {return REALDECLARE;}
"string" {return STRINGDECLARE;}
"struct" {return STRUCTDECLARE;}
"variable" {return VARIABLEDECLARE;}
"function" {return FUNCTIONDECLARE;}
"constant" {return CONSTANT;}
"main" {return MAIN;}
"continue" {return CONTINUE;}
"default" {return DEFAULT;}
"repeat" {return REPEAT;}
"if" {return IF;}
"else" {return ELSE ;}
"for" {return FOR;}
"library" {return LIBRARYDECLARE;}
"while" {return WHILE;}
"return" {return RETURN;}
"switch" {return SWITCH;}
";" {return SEMICOLON;}
"(" {return LEFTPARENTESIS;}
")" {return RIGHTPARENTESIS;}
"[" {return LEFTBRACKET;}
"]" {return RIGHTBRACKET;}
"'" {return SINGLEQUOTE;}
"\"" {return DOUBLEQUOTE;}
":" {return TWOPOINTS;}
"..." {return DOTS;}
"{" {return LEFTKEY;}
"}" {return RIGHTKEY;}
"," {return COMMA;}
"\n" {return LINE;}
{W}({W}|{N})* {lexema=yytext(); return ID;}
. {return ERROR;}