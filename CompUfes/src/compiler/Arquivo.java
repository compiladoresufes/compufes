/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

import Model.Identifier;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Avell B154 PLUS
 */
public class Arquivo {

    private String path;
    private File file;
    private List<Identifier> tokenslist;

    public Arquivo(String path) {
        this.path = path;
        file = new File(path);
        tokenslist = new ArrayList<>();
    }

    public ArrayList<String> getTokenAndLexemaFromFile() throws IOException {

        Reader reader = new BufferedReader(new FileReader(path));
        Lexer lexer = new Lexer(reader);
        ArrayList<String> result = new ArrayList<>();
        String tokenName = "";

        int contIDs = 0;
        int lineCounter = 1;
        boolean flagLine;
        while (true) {
            Token token = lexer.yylex();
            if (token == null) {
                break;
            }
            flagLine = true;
            switch (token) {

                case SUM:
                    tokenName = "+";
                    break;
                case MINUS:
                    tokenName = "-";
                    break;
                case MULT:
                    tokenName = "*";
                    break;
                case DIV:
                    tokenName = "/";
                    break;
                case POW:
                    tokenName = "^";
                    break;
                case ASSIGN:
                    tokenName = "<-";
                    break;
                case ERROR:
                    tokenName = "Erro! Linha: " + lineCounter + ".";
                    break;
                case INTEGERDECLARE:
                    tokenName = "INTEGER";
                    break;
                case ID: {
                    contIDs++;
                    Identifier tokenitem = new Identifier(lexer.lexema, contIDs);
                    //tokenslist.add(tokenitem);
                    tokenName = tokenitem.getName();
                    break;
                }
                case INT:
                    tokenName = lexer.lexema;
                    break;
                case POINTERDECLARE:
                    tokenName = "POINTER->";
                    break;
                case STRUCTDECLARE:
                    tokenName = "STRUCT";
                    break;
                case VARIABLEDECLARE:
                    tokenName = "VARIABLE";
                    break;
                case FUNCTIONDECLARE:
                    tokenName = "FUNCTION";
                    break;
                case LIBRARYDECLARE:
                    tokenName = "LIBRARY";
                    break;
                case MAIN:
                    tokenName = "MAIN";
                    break;
                case STRINGDECLARE:
                    tokenName = "STRING";
                    break;
                case OR_:
                    tokenName = "OR";
                    break;
                case AND_:
                    tokenName = "AND";
                    break;
                case LT:
                    tokenName = "<";
                    break;
                case GT:
                    tokenName = ">";
                    break;
                case NOT:
                    tokenName = "~";
                    break;
                case NOTEQUAL:
                    tokenName = "!=";
                    break;
                case EQUAL:
                    tokenName = "=";
                    break;
                case TRUE:
                    tokenName = "TRUE";
                    break;
                case FALSE:
                    tokenName = "FALSE";
                    break;
                case NULL:
                    tokenName = "NULL";
                    break;
                case BOOLEANDECLARE:
                    tokenName = "BOOLEAN";
                    break;
                case BREAK:
                    tokenName = "BREAK";
                    break;
                case CASE:
                    tokenName = "CASE";
                    break;
                case CHARDECLARE:
                    tokenName = "CHAR";
                    break;
                case CONSTANT:
                    tokenName = "CONSTANT";
                    break;
                case CONTINUE:
                    tokenName = "CONTINUE";
                    break;
                case DEFAULT:
                    tokenName = "DEFAULT";
                    break;
                case REPEAT:
                    tokenName = "REPEAT";
                    break;
                case REAL:
                    tokenName = lexer.lexema;
                    break;
                case REALDECLARE:
                    tokenName = "REAL";
                    break;
                case ELSE:
                    tokenName = "ELSE";
                    break;
                case WHILE:
                    tokenName = "WHILE";
                    break;
                case RETURN:
                    tokenName = "RETURN";
                    break;
                case MOD_:
                    tokenName = "MOD";
                    break;
                case SWITCH:
                    tokenName = "SWITCH";
                    break;
                case FOR:
                    tokenName = "FOR";
                    break;
                case IF:
                    tokenName = "IF";
                    break;
                case SEMICOLON:
                    tokenName = ";";
                    break;
                case LEFTPARENTESIS:
                    tokenName = "(";
                    break;
                case RIGHTPARENTESIS:
                    tokenName = ")";
                    break;
                case LEFTBRACKET:
                    tokenName = "[";
                    break;
                case RIGHTBRACKET:
                    tokenName = "]";
                    break;
                case SINGLEQUOTE:
                    tokenName = "'";
                    break;
                case DOUBLEQUOTE:
                    tokenName = "\"";
                    break;
                case TWOPOINTS:
                    tokenName = ":";
                    break;
                case LEFTKEY:
                    tokenName = "{";
                    break;
                case RIGHTKEY:
                    tokenName = "}";
                    break;
                case COMMA:
                    tokenName = ",";
                    break;
                case LINE:
                    flagLine = true;
                    lineCounter++;
                    break;
                case LTE:
                    tokenName = "<=";
                    break;
                case GTE:
                    tokenName = ">=";
                    break;
                case STRING:
                    tokenName = lexer.lexema;
                    break;
                case DOTS:
                    tokenName = "...";
                    break;
                default:
                    tokenName = lexer.lexema;
            }
            if (flagLine) {
                result.add(token + "#" + tokenName + "#" + lineCounter);
            }

        }
        return result;
    }

    public void gravarArquivo(String texto) throws IOException {
        FileWriter w = new FileWriter(path, false);
        BufferedWriter bf = new BufferedWriter(w);
        bf.write(texto);
        bf.close();
    }

}
